import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatSnackBarModule,
  MatTableModule, MatToolbarModule
} from '@angular/material';
import {OverlayModule} from '@angular/cdk/overlay';

@NgModule({
  imports: [
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    OverlayModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatToolbarModule,
    MatSnackBarModule
  ],
  exports: [
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    OverlayModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatToolbarModule,
    MatSnackBarModule
  ]
})


export class MaterialModule {
}
