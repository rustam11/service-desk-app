import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {BehaviorSubject} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {UpdateTaskModel} from '../pages/tasks-table/tasks-overlay/updatedTask.model';

export interface UserTaskResponseData {
  value: TaskDetail[];
}

export interface TaskDetail {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  price: number;
  taskTypeId: number;
  taskTypeName: string;
  statusId: number;
  statusName: string;
  statusRgb: string;
  priorityId: number;
  priorityName: string;
  serviceId: number;
  serviceName: string;
  resolutionDatePlan: string;
  initiatorId: number;
  initiatorName: string;
  executorId: number;
  executorName: string;
  executorGroupId: number;
  executorGroupName: string;
  tags?: TasksTags[];
  lifetimeItems?: LifetimeItems[];
}

export interface LifetimeItems {
  id: number;
  userName: string;
  lifetimeType: number;
  createdAt: string;
  comment: string;
  fieldName: string;
  oldFieldValue: string;
  newFieldValue: string;
}

export interface TasksTags {
  id: number;
  name: string;
}

export interface User {
  id: number;
  name: string;
}

export interface TaskStatus {
  rgb: string;
  id: number;
  name: string;
}

export interface Priority {
  rgb: string;
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  tasksData = new BehaviorSubject<TaskDetail[]>(null);
  private _lifeTimeItems = new BehaviorSubject<LifetimeItems[]>(null);
  private _tasksDataList: TaskDetail[] = [];

  constructor(private http: HttpClient) {
  }

  get getLifeTimeItems() {
    return this._lifeTimeItems.asObservable();
  }

  getStatuses() {
    return this.http.get<TaskStatus[]>(`${environment.apiUrl}/api/${environment.apiKey}/Statuses`);
  }

  getUsers() {
    return this.http.get<User[]>(`${environment.apiUrl}/api/${environment.apiKey}/Users`);
  }

  getPriorities() {
    return this.http.get<User[]>(`${environment.apiUrl}/api/${environment.apiKey}/Priorities`);
  }

  getTask(id: number) {
    return this.http.get<TaskDetail>
    (`${environment.apiUrl}/api/${environment.apiKey}/Tasks/${id}`, {params: {tenantguid: environment.apiKey}})
      .pipe(
        tap((task: TaskDetail) => {
          this._lifeTimeItems.next(task.lifetimeItems);
        })
      );
  }

  getTasks() {
    return this.http.get<UserTaskResponseData>(`${environment.apiUrl}/odata/tasks`, {params: {tenantguid: environment.apiKey}})
      .pipe(
        map((response: UserTaskResponseData) => {
          const data = response.value;
          this.tasksData.next(data);
          this._tasksDataList = data;
        })
      );
  }

  addNewTask(name: string, description: string) {
    return this.http.post<number>(`${environment.apiUrl}/api/${environment.apiKey}/Tasks`, {
      name,
      description
    }, {params: {tenantguid: environment.apiKey}});
  }

  updateTask(data: UpdateTaskModel) {
    return this.http.put<UpdateTaskModel>(`${environment.apiUrl}/api/${environment.apiKey}/Tasks`, data,
      {
        params: {tenantguid: environment.apiKey}
      });
  }

  updateUI(index: number, newTask: TaskDetail) {
    this._tasksDataList[this._tasksDataList.findIndex(el => el.id === index)] = newTask;
    this.tasksData.next(this._tasksDataList);
  }
}
