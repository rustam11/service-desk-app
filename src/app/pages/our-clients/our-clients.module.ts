import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {OurClientsComponent} from './our-clients.component';
import {FlexLayoutModule} from '@angular/flex-layout';

const routes: Routes = [
  {
    path: '',
    component: OurClientsComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FlexLayoutModule,
  ],
  exports: [RouterModule],
  declarations: [OurClientsComponent]
})
export class OurClientsModule { }
