import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import {BreakpointObserver} from '@angular/cdk/layout';

import {Observable, of, Subject, Subscription} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {UiService} from '../../services/ui.service';
import {Priority, TaskDetail, TasksService, TaskStatus, User} from '../../services/tasks-service.service';
import {TasksOverlayComponent} from './tasks-overlay/tasks-overlay.component';


@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})


export class TasksTableComponent implements OnInit, OnDestroy {

  serviceDeskTasks$: Observable<TaskDetail[]>;
  tasksSub: Subscription;
  usersSub: Subscription;
  statusesSub: Subscription;
  taskSub: Subscription;
  priorSub: Subscription;
  loadingError$ = new Subject<boolean>();

  dialogRef: MatDialogRef<TasksOverlayComponent, any> = null;

  usersList: User[];
  statusesList: TaskStatus[];
  priorities: Priority[];

  // Table setup
  displayedColumns = ['id', 'name', 'statusName', 'executorName'];

  constructor(
    private tasksService: TasksService,
    public dialog: MatDialog,
    private uiService: UiService,
    private breakpointObserver: BreakpointObserver,
    private cdf: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.serviceDeskTasks$ = this.tasksService.tasksData;
    this.tasksSub = this.tasksService.getTasks()
      .pipe(
        catchError((error) => {
          this.uiService.showShackbar('Невозможно получить данные', null, 3000);
          this.loadingError$.next(true);
          return of([]);
        })
      ).subscribe();
    this.fetchData();
  }


  fetchData() {
    this.statusesSub = this.tasksService.getStatuses().subscribe((status: TaskStatus[]) => {
      this.statusesList = status;
    }, error => {
      this.loadingError$.next(true);
      this.uiService.showShackbar('Невозможно получить данные', null, 3000);
    });
    this.usersSub = this.tasksService.getUsers().subscribe((users: User[]) => {
      this.usersList = users;
    }, error => {
      this.loadingError$.next(true);
      this.uiService.showShackbar('Невозможно получить данные', null, 3000);
    });
    this.priorSub = this.tasksService.getPriorities().subscribe((priorities: Priority[]) => {
      this.priorities = priorities;
    }, error => {
      this.loadingError$.next(true);
      this.uiService.showShackbar('Невозможно получить данные', null, 3000);
    });
  }

  detectPriorityColor(id: number) {
    if (this.priorities) {
      const color = this.priorities.filter(el => el.id === id);
      return color[0].rgb;
    }
  }


  myTrackById(index, item) {
    return item.id;
  }

  onCreateTask() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      isEditMode: false
    };
    this.dialogRef = this.dialog.open(TasksOverlayComponent, {
      hasBackdrop: false,
      closeOnNavigation: true,
      width: '975px',
      disableClose: false,
      panelClass: this.breakpointObserver.isMatched('(max-width: 599px)') ? 'task-overlay-fullscreen' : 'task-overlay',
      data: dialogConfig
    });

    this.dialogRef.afterClosed().subscribe(res => {
      if (res && res.data) {
        if (res.role === 'newTask') {
          this.uiService.showShackbar('Заявка успешно создана', null, 3000);
          this.cdf.detectChanges();
          this.onEditTask(+res.data);
          this.tasksSub = this.tasksService.getTasks().subscribe();
        }
      }
    }, error => {
      this.loadingError$.next(true);
      this.uiService.showShackbar('Невозможно получить данные', null, 3000);
    });
  }

  onEditTask(elementId: number) {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
    this.taskSub = this.tasksService.getTask(elementId).subscribe((taskDetail: TaskDetail) => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
        isEditMode: true,
        taskDetail,
        usersList: this.usersList,
        statusesList: this.statusesList,
        priorities: this.priorities
      };
      this.dialogRef = this.dialog.open(TasksOverlayComponent, {
        hasBackdrop: false,
        closeOnNavigation: true,
        width: '975px',
        panelClass: this.breakpointObserver.isMatched('(max-width: 599px)') ? 'task-overlay-fullscreen' : 'task-overlay',
        disableClose: false,
        data: dialogConfig
      });
      this.dialogRef.afterClosed().subscribe(() => {
        this.cdf.detectChanges();
      }, error => {
        this.uiService.showShackbar('Невозможно получить данные', null, 3000);
        this.loadingError$.next(true);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.tasksSub) {
      this.tasksSub.unsubscribe();
    }
    if (this.taskSub) {
      this.taskSub.unsubscribe();
    }
    if (this.usersSub) {
      this.usersSub.unsubscribe();
    }
    if (this.statusesSub) {
      this.statusesSub.unsubscribe();
    }
    if (this.priorSub) {
      this.priorSub.unsubscribe();
    }
  }
}
