import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BreakpointObserver} from '@angular/cdk/layout';

import {switchMap} from 'rxjs/operators';
import {Observable, Subscription} from 'rxjs';
import {UiService} from '../../../services/ui.service';
import {LifetimeItems, TaskDetail, TasksService, TaskStatus, User} from '../../../services/tasks-service.service';
import {UpdateTaskModel} from './updatedTask.model';


@Component({
  selector: 'app-tasks-overlay',
  templateUrl: './tasks-overlay.component.html',
  styleUrls: ['./tasks-overlay.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class TasksOverlayComponent implements OnInit, OnDestroy {
  taskForm: FormGroup;
  taskEditForm: FormGroup;

  usersList: User[];
  statusesList: TaskStatus[];
  selectedStatus: TaskStatus;
  taskDetail: TaskDetail;
  selectedExecutor: User;
  lifeTimeItems$: Observable<LifetimeItems[]> = null;

  taskSub: Subscription;

  isEditMode: boolean;
  taskId: number;
  selectedStatusColor = '';
  titleLength = 130;

  constructor(
    private tasksService: TasksService,
    private uiService: UiService,
    public dialogRef: MatDialogRef<TasksOverlayComponent>,
    private breakpointObserver: BreakpointObserver,
    @Inject(MAT_DIALOG_DATA) public data: TasksOverlayComponent
  ) {
    this.isEditMode = data.data.isEditMode;
    if (this.isEditMode) {
      this.taskDetail = data.data.taskDetail;
      this.statusesList = data.data.statusesList;
      this.usersList = data.data.usersList;
    }
  }

  ngOnInit() {
    if (this.breakpointObserver.isMatched('(max-width: 992px )') &&
      this.breakpointObserver.isMatched('(min-width: 768px)')) {
      this.titleLength = 90;
    }
    if (this.breakpointObserver.isMatched('(max-width: 599px )') &&
      this.breakpointObserver.isMatched('(min-width: 374px)')) {
      this.titleLength = 20;
    }
    if (this.breakpointObserver.isMatched('(max-width: 373px)')) {
      this.titleLength = 10;
    }
    this.setupNewRequestForm();
    if (this.isEditMode) {
      this.selectedStatusColor = this.taskDetail.statusRgb;
      this.lifeTimeItems$ = this.tasksService.getLifeTimeItems;
    }
  }

  onStatusChange() {
    const statusesArr = this.statusesList.filter(el => el.id === this.taskEditForm.value.status);
    this.selectedStatusColor = statusesArr[0].rgb;
    this.selectedStatus = statusesArr[0];
  }

  onExecutorChange() {
    const executorsArr = this.usersList.filter(el => el.id === this.taskEditForm.value.executorName);
    this.selectedExecutor = executorsArr[0];
  }

  setupNewRequestForm() {
    if (!this.isEditMode) {
      this.taskForm = new FormGroup({
        name: new FormControl(
          '',
          {
            updateOn: 'change',
            validators: [Validators.required]
          }
        ),
        description: new FormControl(
          '',
          {
            updateOn: 'change',
            validators: [Validators.required]
          }
        ),
      });
    } else {
      this.taskEditForm = new FormGroup({
        comment: new FormControl(
          '',
          {
            updateOn: 'change',
            validators: [Validators.required, Validators.minLength(5), Validators.maxLength(150)]
          }
        ),
        status: new FormControl(this.taskDetail.statusId,
          {
            updateOn: 'change',
            validators: [Validators.required]
          }
        ),
        executorName: new FormControl(
          this.taskDetail.executorId,
          {
            updateOn: 'change',
            validators: [Validators.required]
          }
        )
      });
    }
  }

  submitNewRequestForm() {
    if (!this.isEditMode) {
      this.taskSub = this.tasksService.addNewTask(this.taskForm.value.name, this.taskForm.value.description).subscribe(res => {
        this.taskId = res;
        this.dialogRef.close({data: this.taskId, role: 'newTask'});
      }, error => {
        this.uiService.showShackbar('Невозможно отправить данные', null, 3000);
        this.onClose();
      });
    } else {
      const updatedRequest = new UpdateTaskModel(
        this.taskDetail.id,
        this.taskDetail.name,
        this.taskDetail.description,
        this.taskEditForm.value.comment,
        this.taskDetail.price,
        this.taskDetail.taskTypeId,
        this.taskEditForm.value.status,
        this.taskDetail.priorityId,
        this.taskDetail.serviceId,
        this.taskDetail.resolutionDatePlan,
        this.taskDetail.initiatorId,
        this.taskEditForm.value.executorName,
        this.taskDetail.executorGroupId,
      );

      this.onStatusChange();
      this.onExecutorChange();
      this.taskDetail.executorName = this.selectedExecutor.name;
      this.taskDetail.executorId = this.selectedExecutor.id;
      this.taskDetail.statusName = this.selectedStatus.name;
      this.taskDetail.statusRgb = this.selectedStatus.rgb;
      this.taskDetail.serviceId = this.selectedStatus.id;

      this.taskSub = this.tasksService.updateTask(updatedRequest)
        .pipe(
          switchMap(() => {
            return this.tasksService.getTask(this.taskDetail.id);
          })
        )
        .subscribe(() => {
        this.taskEditForm.patchValue({comment: ''});
        this.tasksService.updateUI(this.taskDetail.id, this.taskDetail);
        this.uiService.showShackbar('Заявка успешно сохранена', null, 3000);
      }, error => {
        this.uiService.showShackbar('Невозможно отправить данные', null, 3000);
        this.onClose();
      });
    }
  }

  onClose() {
    this.dialogRef.close({data: 'close'});
  }

  trackByCommentFn(index, item: LifetimeItems) {
    return item.id;
  }

  ngOnDestroy(): void {
    if (this.taskSub) {
      this.taskSub.unsubscribe();
    }
  }

}
