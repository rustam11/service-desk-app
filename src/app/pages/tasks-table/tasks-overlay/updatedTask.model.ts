export class UpdateTaskModel {

  constructor(
    public id: number,
    public name: string,
    public description: string,
    public comment: string,
    public price: number,
    public taskTypeId: number,
    public statusId: number,
    public priorityId: number,
    public serviceId: number,
    public resolutionDatePlan: string,
    private initiatorId: number,
    private executorId: number,
    private executorGroupId: number,
  ) {
  }
}
