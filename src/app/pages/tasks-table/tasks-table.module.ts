import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';

import {PipesModule} from '../../pipe/pipes.module';
import {MaterialModule} from '../../material.module';
import {TasksTableComponent} from './tasks-table.component';
import {TasksOverlayComponent} from './tasks-overlay/tasks-overlay.component';
import {ComponentsModule} from '../../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: TasksTableComponent
  }
];


@NgModule({
  declarations: [TasksTableComponent, TasksOverlayComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    PipesModule,
    ComponentsModule
  ],
  exports: [RouterModule],
  entryComponents: [TasksTableComponent, TasksOverlayComponent ],
})
export class TasksTableModule {
}
