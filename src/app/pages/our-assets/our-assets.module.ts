import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {OurAssetsComponent} from './our-assets.component';
import {FlexLayoutModule} from '@angular/flex-layout';

const routes: Routes = [
  {
    path: '',
    component: OurAssetsComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FlexLayoutModule,
  ],
  exports: [RouterModule],
  declarations: [OurAssetsComponent]
})
export class OurAssetsModule { }
