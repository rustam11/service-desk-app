import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {DataBaseComponent} from './data-base.component';
import {FlexLayoutModule} from '@angular/flex-layout';

const routes: Routes = [
  {
    path: '',
    component: DataBaseComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FlexLayoutModule,
  ],
  exports: [RouterModule],
  declarations: [DataBaseComponent]
})
export class DataBaseModule { }
