import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import {HomeComponent} from './pages/home/home.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'tasks',
    loadChildren: () => import('./pages/tasks-table/tasks-table.module').then(m => m.TasksTableModule)
  },
  {
    path: 'our-clients',
    loadChildren: () => import('./pages/our-clients/our-clients.module').then(m => m.OurClientsModule)
  },
  {
    path: 'our-assets',
    loadChildren: () => import('./pages/our-assets/our-assets.module').then(m => m.OurAssetsModule)
  },
  {
    path: 'data-base',
    loadChildren: () => import('./pages/data-base/data-base.module').then(m => m.DataBaseModule)
  },
  {
    path: 'our-employees',
    loadChildren: () => import('./pages/employee/employee.module').then(m => m.EmployeeModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
