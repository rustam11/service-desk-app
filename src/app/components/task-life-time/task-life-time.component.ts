import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

import {LifetimeItems} from '../../services/tasks-service.service';

@Component({
  selector: 'app-task-life-time',
  templateUrl: './task-life-time.component.html',
  styleUrls: ['./task-life-time.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskLifeTimeComponent implements OnInit {
  @Input() staffComment: LifetimeItems | null = null;

  constructor() { }

  ngOnInit() {
  }

}
