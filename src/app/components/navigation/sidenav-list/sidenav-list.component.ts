import { Component, OnInit } from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      `check-mark`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/check-mark.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `data-base`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/data-base.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `clients`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/clients.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `analytics`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/analytics.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `employees`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/employees.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `settings`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/settings.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `tasks`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/tasks.svg')
    );
  }

  ngOnInit() {
  }
}
