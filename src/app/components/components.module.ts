import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';

import {HeaderComponent} from './navigation/header/header.component';
import {SidenavListComponent} from './navigation/sidenav-list/sidenav-list.component';
import {TaskLifeTimeComponent} from './task-life-time/task-life-time.component';
import {MaterialModule} from '../material.module';

@NgModule({
  declarations: [
    HeaderComponent,
    SidenavListComponent,
    TaskLifeTimeComponent
  ],
  imports: [
    MaterialModule,
    RouterModule,
    CommonModule,
    FlexLayoutModule
  ],
  exports: [
    HeaderComponent,
    SidenavListComponent,
    TaskLifeTimeComponent
  ]
})

export class ComponentsModule {
} 
