import {NgModule} from '@angular/core';
import {ShortenPipe} from './shorten-pipe.pipe';

@NgModule({
  declarations: [ShortenPipe],
  exports: [ShortenPipe],
})

export class PipesModule {}
